;; Trigonometric functions
;; Liam Healy Wed Dec  7 2005 - 22:53
;; Time-stamp: <2017-11-17 17:57:20EST trigonometry.lisp>

;; Copyright 2017 Liam M. Healy
;; Distributed under the terms of the GNU General Public License
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(in-package :antik)

(export '(angle-law-of-cosines length-law-of-cosines))

(defun angle-law-of-cosines
    (length-opp length-adj1 length-adj2)
  "Solve for the angle when lengths of a triangle are known."
  (acos (/ (+ (expt length-adj1 2) (expt length-adj2 2)
	      (- (expt length-opp 2)))
	   (* 2 length-adj1 length-adj2))))

(defun length-law-of-cosines
    (angle length-adj1 length-adj2)
  "Solve for the opposite length when angle and adjacent lengths of a triangle are known."
  (sqrt (+ (expt length-adj1 2)
	   (expt length-adj2 2)
	   (* -2 length-adj1 length-adj2 (cos angle)))))
